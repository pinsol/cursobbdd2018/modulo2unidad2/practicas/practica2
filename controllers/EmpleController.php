<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\db\Query;


/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    // Mostrar todos los campos y todos los 
    // registros de la tabla empleado
    public function actionConsulta1(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find();
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
       
       /** 
        * ejecuto la consulta
        * array de activeRecord (un array de modelos)
        */
       
       $resultado=$r->all();
       
       
       /**
        * crear una consulta con QueryBuilder
        * 
        */
       
        $listado= (new Query())
                ->select("*")
                ->from("emple")
                ->all();
        
        /**
         * Crear una consulta con el objeto conexion
         */
        
        $consulta=Yii::$app->db
                ->createCommand("select * from emple")
                ->queryAll();
       
       return $this->render("consulta1",[
           "datos"=>$resultado,
           "dataProvider"=>$d,
           "listado"=>$listado,
           "consulta"=>$consulta,
       ]);
        
    }
    // Mostrar el apellido y oficio de cada empleado.
    public function actionConsulta3(){
       
        $consulta= Emple::find()
                ->select("apellido,oficio");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta3",[
            "datos"=>$dp
        ]);
    }
    // Indicar el numero de empleados que hay.
     public function actionConsulta6(){
       
        $consulta= Emple::find()
                ->count();
        
        
        return $this->render("consulta6",[
            "datos"=>$consulta,
        ]);
    }
    // Datos de los empleados ordenados por apellido de forma ascendente.
     public function actionConsulta7(){
       
        $consulta= Emple::find()
                ->orderBy("apellido");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta7",[
            "datos"=>$dp
        ]);
    }
    // Datos de los empleados ordenados por apellido de forma descendente.
     public function actionConsulta8(){
       
        $consulta= Emple::find()
                ->orderBy(["apellido" => SORT_DESC]);
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta8",[
            "datos"=>$dp
        ]);
    }
    // Datos de los empleados ordenados por número de departamento descendentemente.
     public function actionConsulta10(){
       
        $consulta= Emple::find()
                ->orderBy(["dept_no" => SORT_DESC]);
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta,
                "pagination"=>[
                    "pageSize"=>5
                ],
       ]);
        
        return $this->render("consulta10",[
            "datos"=>$dp
        ]);
    }
    // Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente.
     public function actionConsulta11(){
       
        $consulta= Emple::find()
                ->orderBy([
                    "dept_no" => SORT_DESC,
                    "oficio" => SORT_ASC
                    ]);
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta11",[
            "datos"=>$dp
        ]);
    }
    // Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente.
     public function actionConsulta12(){
       
        $consulta= Emple::find()
                ->orderBy([
                    "dept_no" => SORT_DESC,
                    "apellido" => SORT_ASC
                    ]);
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta12",[
            "datos"=>$dp
        ]);
    }
    /*
    // Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.
    public function actionConsulta13(){
       
        $consulta= Emple::find()
                ->select("emp_no")
                ->where("salario>2000");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta13",[
            "datos"=>$dp
        ]);
    } */
    
    //Mostrar los códigos de los empleados cuyo salario sea mayor que 2000
    public function actionConsulta13(){
       $consulta= Emple::find()
               ->select("emp_no")
               ->where("salario>2000");
       
       $dp=new ActiveDataProvider([
           "query"=>$consulta
       ]);
       
       return $this->render("consulta13",[
          "datos"=>$dp 
       ]);
    }
    // Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000.
    public function actionConsulta14(){
       
        $consulta= Emple::find()
                ->select("emp_no,
                        apellido")
                ->where("salario<2000");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta14",[
            "datos"=>$dp
        ]);
    }
    // Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500.
    public function actionConsulta15(){
       
        $consulta= Emple::find()
                ->where("salario between 1500 and 2500");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta15",[
            "datos"=>$dp
        ]);
    }
    // Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ.
    public function actionConsulta16(){
       
        $consulta= Emple::find()
                ->where("oficio like 'ANALISTA'");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta16",[
            "datos"=>$dp
        ]);
    }
    // Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €.
    public function actionConsulta17(){
       
        $consulta= Emple::find()
                ->where("oficio like 'analista' and
                        salario>2000");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta17",[
            "datos"=>$dp
        ]);
    }
    // Seleccionar el apellido y oficio de los empleados del departamento número 20.
    public function actionConsulta18(){
       
        $consulta= Emple::find()
                ->select("apellido,
                        oficio")
                ->where("dept_no=20");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta18",[
            "datos"=>$dp
        ]);
    }
    // Contar el número de empleados cuyo oficio sea VENDEDOR.
     public function actionConsulta19(){
       
        $consulta= Emple::find()
                ->where("oficio like 'VENDEDOR'")
                ->count();
        
        
        return $this->render("consulta19",[
            "datos"=>$consulta,
        ]);
    }
    // Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente
     public function actionConsulta20(){
       
        $consulta= Emple::find()
                ->Where(["or",("apellido like 'm%'"),("apellido like 'n%'")])
                ->orderBy([
                    "apellido" => SORT_ASC
                    ]);
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta20",[
            "datos"=>$dp
        ]);
    }
    // Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma ascendente.
     public function actionConsulta21(){
       
        $consulta= Emple::find()
                ->Where("oficio like 'VENDEDOR'")
                ->orderBy([
                    "apellido" => SORT_ASC
                    ]);
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta21",[
            "datos"=>$dp
        ]);
    }
    // Mostrar los apellidos del empleado que mas gana.
     public function actionConsulta22(){
       
         $max = Emple::find()
                 ->max("salario");
         
        $consulta= Emple::find()
                ->where(["salario"=>$max])
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta22",[
            "datos"=>$dp
        ]);
    }
    // Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y oficio de forma ascendente
    public function actionConsulta23(){
       
        $consulta= Emple::find()
                ->where(['AND',("dept_no=10"),("oficio like 'ANALISTA'")])
                ->orderBy(["apellido"=>SORT_ASC,"oficio"=>SORT_ASC])
                    ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta23",[
            "datos"=>$dp
        ]);
    }
    // Realizar un listado de los distintos meses en que los empleados se han dado de alta
    public function actionConsulta24(){
       
        $consulta= Emple::find()
                ->select('month(fecha_alt) as salida')
                ->distinct('salida')
                    ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta24",[
            "datos"=>$dp
        ]);
    }
    // Realizar un listado de los distintos años en que los empleados se han dado de alta
    public function actionConsulta25(){
       
        $consulta= Emple::find()
                ->select('year(fecha_alt) as salida')
                ->distinct('salida')
                ->orderBy('salida')
                    ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta25",[
            "datos"=>$dp
        ]);
    }
    // Realizar un listado de los distintos dias del mes en que los empleados se han dado de alta
    public function actionConsulta26(){
       
        $consulta= Emple::find()
                ->select('day(fecha_alt) as salida')
                ->distinct('salida')
                ->orderBy('salida')
                    ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta26",[
            "datos"=>$dp
        ]);
    }
    // Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20.

    public function actionConsulta27(){
       
        $consulta= Emple::find()
                ->select('apellido')
                ->where(["or","salario>2000","dept_no=20"])
                    ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta27",[
            "datos"=>$dp
        ]);
    }
    // Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece.

    public function actionConsulta28(){
       
        $consulta= Emple::find()
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta28",[
            "datos"=>$dp
        ]);
    }
    // Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del
    // departamento al que pertenece. Ordenar los resultados por apellido de forma descendente.

    public function actionConsulta29(){
       
        $consulta= Emple::find()
                ->orderBy(["apellido"=>SORT_DESC])
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta29",[
            "datos"=>$dp
        ]);
    }
    // Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a continuación.

    public function actionConsulta30(){
       
        $consulta= Emple::find()
                ->select("dept_no,count(*) as salida")
                ->groupBy("dept_no");
                
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta30",[
            "datos"=>$dp
        ]);
    }
    // Listar el número de empleados por departamento. Cambiando la salida.

    public function actionConsulta31(){
       
        $consulta= Emple::find()
                ->select("dept_no,count(*) as salida")
                ->groupBy("dept_no");
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta31",[
            "datos"=>$dp
        ]);
    }
    // Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre.

    public function actionConsulta32(){
       
        $consulta= Emple::find()
                ->select ("apellido")
                ->orderBy(["oficio"=>SORT_ASC,"apellido"=>SORT_ASC]) 
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta32",[
            "datos"=>$dp
        ]);
    }
    // Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ. Listar el apellido de los empleados.

    public function actionConsulta33(){
       
        $consulta= Emple::find()
               ->select ("apellido")
               ->where ("apellido like 'a%'")
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta33",[
            "datos"=>$dp
        ]);
    }
    // Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. Listar el apellido de los empleados.

    public function actionConsulta34(){
       
        $consulta= Emple::find()
               ->select ("apellido")
               ->where (["or","apellido like 'a%'","apellido like 'm%'"])
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta34",[
            "datos"=>$dp
        ]);
    }
    // Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. Listar todos los campos de la tabla empleados.

    public function actionConsulta35(){
       
        $consulta= Emple::find()
                 ->where ("apellido not like '%z'")
                ;
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta35",[
            "datos"=>$dp
        ]);
    }
    
}
