<?php
    use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Práctica 2';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Práctica 2</h1>

        <p class="lead"><h2>Consultas</h2></p>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-md-2">
                Consulta 1.-
            </div>
            <div class="col-md-8">
               Mostrar todos los campos y todos los registros de la tabla empleado.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta1'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Consulta 2.-
            </div>
            <div class="col-md-8">
            Mostrar todos los campos y todos los registros de la tabla departamento.               
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['depart/consulta2'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Consulta 3.-
            </div>
            <div class="col-md-8">
            Mostrar el apellido y oficio de cada empleado.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta3'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Consulta 4.-
            </div>
            <div class="col-md-8">
            Mostrar localización y número de cada departamento.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['depart/consulta4'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>      
        <div class="row">
            <div class="col-md-2">
                Consulta 5.-
            </div>
            <div class="col-md-8">
            Mostrar el número, nombre y localización de cada departamento.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['depart/consulta5'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>      
        
        <div class="row">
            <div class="col-md-2">
                Consulta 6.-
            </div>
            <div class="col-md-8">
            Indicar el numero de empleados que hay.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta6'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 7.-
            </div>
            <div class="col-md-8">
            Datos de los empleados ordenados por apellido de forma ascendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta7'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 8.-
            </div>
            <div class="col-md-8">
            Datos de los empleados ordenados por apellido de forma desscendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta8'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
         <div class="row">
            <div class="col-md-2">
                Consulta 9.-
            </div>
            <div class="col-md-8">
            Indicar el numero de departamentos que hay.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['depart/consulta9'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 10.-
            </div>
            <div class="col-md-8">
            Datos de los empleados ordenados por número de departamento descendentemente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta10'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 11.-
            </div>
            <div class="col-md-8">
            Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta11'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 12.-
            </div>
            <div class="col-md-8">
            Datos de los empleados ordenados por número de departamento descendentemente y por apellido
                ascendentemente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta12'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 13.-
            </div>
            <div class="col-md-8">
            Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta13'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 14.-
            </div>
            <div class="col-md-8">
            Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta14'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 15.-
            </div>
            <div class="col-md-8">
            Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta15'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 16.-
            </div>
            <div class="col-md-8">
            Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta16'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 17.-
            </div>
            <div class="col-md-8">
            Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta17'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 18.-
            </div>
            <div class="col-md-8">
            Seleccionar el apellido y oficio de los empleados del departamento número 20.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta18'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 19.-
            </div>
            <div class="col-md-8">
            Contar el número de empleados cuyo oficio sea VENDEDOR.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta19'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 20.-
            </div>
            <div class="col-md-8">
            Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta20'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 21.-
            </div>
            <div class="col-md-8">
            Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma ascendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta21'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 22.-
            </div>
            <div class="col-md-8">
            Mostrar los apellidos del empleado que mas gana.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta22'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 23.-
            </div>
            <div class="col-md-8">
            Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y oficio de forma ascendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta23'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 24.-
            </div>
            <div class="col-md-8">
            Realizar un listado de los distintos meses en que los empleados se han dado de alta.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta24'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 25.-
            </div>
            <div class="col-md-8">
            Realizar un listado de los distintos años en que los empleados se han dado de alta.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta25'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 26.-
            </div>
            <div class="col-md-8">
            Realizar un listado de los distintos dias en que los empleados se han dado de alta.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta26'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 27.-
            </div>
            <div class="col-md-8">
            Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta27'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 28.-
            </div>
            <div class="col-md-8">
            Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta28'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 29.-
            </div>
            <div class="col-md-8">
            Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del
            departamento al que pertenece. Ordenar los resultados por apellido de forma descendente.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta29'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 30.-
            </div>
            <div class="col-md-8">
            Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a
            continuación.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta30'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 31.-
            </div>
            <div class="col-md-8">
            Listar el número de empleados por departamento. Cambiando la salida.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta31'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 32.-
            </div>
            <div class="col-md-8">
            Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta32'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 33.-
            </div>
            <div class="col-md-8">
            Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ. Listar el apellido de los empleados.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta33'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 34.-
            </div>
            <div class="col-md-8">
            Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. Listar el apellido de los
            empleados.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta34'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                Consulta 35.-
            </div>
            <div class="col-md-8">
            Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. Listar todos los campos de la tabla
            empleados.
            </div>
            <div class="col-md-2 pad-1">
            <?= Html::a('Ejecutar', ['emple/consulta35'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        
    </div>
</div>
