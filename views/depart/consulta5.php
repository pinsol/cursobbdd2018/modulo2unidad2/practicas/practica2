<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consulta 5';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= Html::encode($this->title) ?></h1>
   
    <h2>Con GridView</h2>
    <?= GridView::widget([
        'dataProvider' => $sergio,
        'columns' => [
            'dept_no',
            'loc',
            'dnombre'
        ],
    ]); ?>
   
</div>
