<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consulta 13';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= Html::encode($this->title) ?></h1>
   
    
<?= 
    ListView::widget([
        'dataProvider' => $datos,
        'itemView' => '_consulta13',
    ]);
?>
    
</div>
